astroid==2.3.3
attrs==19.3.0
backcall==0.1.0
bleach==3.1.4
certifi==2020.6.20
colorama==0.4.3
cycler==0.10.0
decorator==4.4.2
defusedxml==0.6.0
entrypoints==0.3
ipykernel==5.2.1
ipython==7.13.0
ipython-genutils==0.2.0
ipywidgets==7.5.1
isort==4.3.21
jedi==0.17.0
Jinja2==2.11.2
joblib @ file:///tmp/build/80754af9/joblib_1601912903842/work
jsonschema==3.2.0
jupyter==1.0.0
jupyter-client==6.1.3
jupyter-console==6.1.0
jupyter-core==4.6.3
kiwisolver==1.2.0
lazy-object-proxy==1.4.3
MarkupSafe==1.1.1
matplotlib @ file:///C:/ci/matplotlib-base_1597876438601/work
mccabe==0.6.1
mistune==0.8.4
mkl-fft==1.2.0
mkl-random==1.1.1
mkl-service==2.3.0
nbconvert==5.6.1
nbformat==5.0.6
notebook==6.0.3
numpy @ file:///C:/ci/numpy_and_numpy_base_1596215850360/work
olefile==0.46
pandocfilters==1.4.2
parso==0.7.0
pickleshare==0.7.5
Pillow @ file:///C:/ci/pillow_1594298230227/work
prometheus-client==0.7.1
prompt-toolkit==3.0.5
Pygments==2.6.1
pylint==2.4.4
pyparsing==2.4.7
pyrsistent==0.16.0
python-dateutil==2.8.1
pywin32==227
pywinpty==0.5.7
pyzmq==19.0.0
qtconsole==4.7.3
QtPy==1.9.0
scikit-learn @ file:///C:/ci/scikit-learn_1598377018496/work
scipy @ file:///C:/ci/scipy_1592916963468/work
Send2Trash==1.5.0
sip==4.19.13
six==1.15.0
terminado==0.8.3
testpath==0.4.4
threadpoolctl @ file:///tmp/tmp9twdgx9k/threadpoolctl-2.1.0-py3-none-any.whl
tornado==6.0.4
traitlets==4.3.3
wcwidth==0.1.9
webencodings==0.5.1
widgetsnbextension==3.5.1
wincertstore==0.2
wrapt==1.11.2

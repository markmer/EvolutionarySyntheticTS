from EvolutionaryModel import ExprModel as em
from evolution.optimizers.Individ import *




# Prepare your time series
ind = Individ([Sin([0.5, 0.08, 0]), Imp([1, 0.01, 0.8 ,0.5, 0.4, 0, 0]), Power([0.01, 1])])
t = np.arange(0, 2000, 1)
# target Time Series must be centered and normalized
target_TS = ind.value(t)



model = em()
model.set_metaparams(full_imp_optimize=False, regularization_coef=0.1)
target, _, _ = em.normalize(target_TS)

best, info = model.fit(target)
best.show(t, target, True)

val = model.get_synthetic(len(target))

plt.figure('synthetic')
plt.plot(val)
plt.show()

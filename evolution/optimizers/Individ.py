import sys, os
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from Tokens import *
from TokenChoiser import *
from scipy.optimize import differential_evolution
from scipy.optimize import minimize, leastsq, differential_evolution, shgo, dual_annealing, basinhopping, brute
from sklearn.linear_model import Lasso, LinearRegression
import time as tm
import sys
from time import perf_counter
import copy
import warnings






class Individ:
	"""
	An object of this class is an individual in the population.
	It has a chromosome consisting of tokens.
	It contains an expression that approximates the input data and is used as a basis for building synthetics.
	"""

	def __init__(self, chromo=None, fit=None, opt_by_lasso=False, kind='init', tokens=None, used_fitness=None, used_value='Plus', \
				use_fix_funcs_in_val=True):
		if tokens == None:
			tokens = Function.__subclasses__()
			for idx, _ in enumerate(tokens):
				tokens[idx] = tokens[idx]()
		if chromo == None:
			chromo = []
		else:
			chromo_types = list(map(lambda x: type(x), chromo))
			tokens_types = list(map(lambda x: type(x), tokens))
			tokens_types.append(type(ImpComplex()))
			for gen in chromo_types:
				if gen not in tokens_types:
					raise KeyError('There are no such tokens that you use in chromo') 
		self.chromo = chromo
		self.fit = fit
		self.opt_by_lasso = opt_by_lasso
		self.kind = kind
		self.fix = False
		self.tokens = tokens
		self.used_value = used_value
		self.used_fitness = used_fitness
		self.use_fix_funcs_in_val = use_fix_funcs_in_val

	def copy(self):
		ind = deepcopy(self)
		ind.opt_by_lasso = False
		self.fix = False
		return ind
		
	def init_formula(self, t, target_TS, max_len = 2):
		len_formula = np.random.randint(1, max_len+1)
		ind = self.mutation(t, target_TS, increase_prob=1, mut_intensive=len_formula)[0]
		self.chromo = ind.chromo

	def formula(self, with_params=False):
		if self.used_value == "plus":
			joinWith = '+'
		else:
			joinWith = '*'
		return joinWith.join(list(map(lambda x: x.name(with_params), self.chromo)))
	
	def name(self, with_params=True):
		s = type(self).__name__ + '('
		s += 'chromo=['
		for gen in self.chromo:
			s += gen.name(with_params)
			s += ','
		s = s[:-1]
		s += '], used_value="' + self.used_value + '")'
		return s

	def show(self, t, target_TS=[], with_patterns=False):
		if len(target_TS) == 0:
			target_TS = np.zeros(len(t))
		plt.figure(self.formula(True))
		if with_patterns:
			plt.subplot(2,1,1)
		plt.plot(t, target_TS, label='target', color='black')
		plt.plot(t, self.value(t), label='model', color='orange')
		# plt.title(self.formula(True), fontdict={'fontsize': 10})
		plt.grid(True)
		plt.xlabel('t')
		plt.ylabel('time series')
		if with_patterns:
			plt.subplot(2,1,2)
			mn = 0
			for idx in range(len(self.chromo)):
				gen = self.chromo[idx]
				if idx > 0:
					mn = self.chromo[idx-1].params[0]
				plt.plot(t, gen.value(t) + 2*mn, label=gen.name(True))
			plt.title('patterns')
			plt.xlabel('t')
			plt.ylabel('pattern')
			plt.grid(True)
			# plt.legend()
		# plt.legend()

	def value(self, t):
		if self.use_fix_funcs_in_val:
			temp = self.chromo
		else:
			temp = list(filter(lambda x: x.fix == False, self.chromo))
		if len(temp) != 0:
			if self.used_value == 'Plus':
				return reduce(lambda val, x: val + x, list(map(lambda x: x.value(t), temp)))
			elif self.used_value == 'Product':
				return reduce(lambda val, x: val * x, list(map(lambda x: x.value(t), temp)))
		return np.zeros(len(t))

	def unfix_all_tokens(self):
		for gen in self.chromo:
			gen.fix_val = False

	@staticmethod	
	def  _val_for_fit(val, target_TS):
		return np.linalg.norm(val - target_TS)/np.linalg.norm(target_TS)

	def fitness(self, *args):
		t, target_TS = args
		if self.used_fitness == None:
			self.fit = self._val_for_fit(self.value(t), target_TS)
		else:
			self.fit = self.used_fitness(t, target_TS)
		return self.fit

	def get_params(self, _type=None):
		chromo = list(filter(lambda x: ((x.type==_type or _type==None) and not x.fix), self.chromo))
		params = []
		for i in chromo:
			params.append(list(i.get_params()))
		return np.array(params)

	def put_params(self, params, *_type):
		if len(_type)==0:
			_type == None
		chromo = list(filter(lambda x: ((x.type in _type or _type==None) and not x.fix), self.chromo))
		start = 0
		for gen in chromo:
			gen.put_params(params[start: start + gen.number_params])
			start += gen.number_params

	def get_chromo(self, types=None):
		if types == None:
			return copy.copy(self.chromo)
		return list(filter(lambda x: x.type in types, self.chromo))

	def del_correlate_tokens(self, t):
		del_chromo = set()
		for idx, gen1 in enumerate(self.chromo):
			for _, gen2 in enumerate(self.chromo[idx+1:]):
				K = np.cov(gen1.value(t), gen2.value(t))
				with warnings.catch_warnings():
					corr = np.abs(K[1, 0]/((K[0, 0]*K[1, 1])**0.5))
					if corr > 0.97:
						del_chromo.add(gen2)
		self.chromo = list(filter(lambda gen: gen not in del_chromo, self.chromo))

	def del_nonuniq_functions(self):
		self.chromo = list(set(self.chromo))

	def del_low_functions(self, t):
		# delete token if its value eq zero
		self.del_nonuniq_functions()
		self.chromo = list(filter(lambda gen: (gen.value(t) != 0).any(), self.chromo))	

	def imp_to_complex_imp(self, t, target_TS=None, stop_idx=None):
		wmax = np.fft.fftfreq(10*len(t), t[1] - t[0]).max()
		if stop_idx == None or stop_idx > len(self.chromo)-1 or stop_idx < 0:
			stop_idx = len(self.chromo)-1
		for idx in range(stop_idx+1):
			token = self.chromo[idx]
			if type(token) in imp_relations.keys() and token.params[1] < 0.1*wmax :
				self.chromo[idx] = ImpComplex(token)
				self.chromo[idx].init_imps(t)

	def mutation(self, t, target_TS, increase_prob=0.7, mut_intensive=2):
		ind = self.copy()
		ind.kind = 'mutation'
		# define how many tokens will be added
		mut_intensive = random.randint(1, mut_intensive)

		# add_funcs are added by special class working with FreqProc!!!
		# add_functions = random.choices(self.tokens, k=mut_intensive)
		add_functions = TokenChoiser.choice_token(t, target_TS-self.value(t), self.tokens, len_top_freqs=2, choice_size=mut_intensive)

		# create new token-object
		for idx in range(len(add_functions)):
			add_functions[idx] = add_functions[idx].copy()
		# tokens is added to the chromo/ replace tokens in chromo/ both variants
		if np.random.uniform() < increase_prob:
			ind.chromo.extend(add_functions)
		else:
			for idx in range(len(ind.chromo)):
				if add_functions:
					ind.chromo[idx] = add_functions.pop(random.randint(0, len(add_functions)-1))
				else:
					break
			# if len(add_functions) > len(chromo) we add token to the chromo
			if add_functions:
				ind.chromo.extend(add_functions)
		return [ind]

	def crossover(self, ind2, increase_prob=0.5, cross_intensive=2):
		ind1 = self.copy()
		ind2 = ind2.copy()

		ind1.kind = 'crossover'
		ind2.kind = 'crossover'

		# inds change tokens or add them to each other depending on increase prob
		# tokens remain fixed!
		cross_intensive = random.randint(1, np.min([cross_intensive, len(ind1.chromo), len(ind2.chromo)]))
		add_idxs1 = np.random.choice(len(ind1.chromo), size=cross_intensive, replace=False)
		add_idxs2 = np.random.choice(len(ind2.chromo), size=cross_intensive, replace=False)
		if np.random.uniform() < increase_prob:
			for idx in add_idxs1:
				gen = ind1.chromo[idx].copy()
				ind2.chromo.append(gen)
			for idx in add_idxs2:
				gen = ind2.chromo[idx].copy()
				ind1.chromo.append(gen)
		else:
			for idx1, idx2 in np.transpose([add_idxs1, add_idxs2]):
				ind1.chromo[idx1], ind2.chromo[idx2] = ind2.chromo[idx2], ind1.chromo[idx1]
		return [ind1, ind2]


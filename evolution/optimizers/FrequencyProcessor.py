import numpy as np
import matplotlib.pyplot as plt

class FrequencyProcessor:
    """
    Works with the spectrum of the studied series.
    Identifies the most significant harmonics to determine the frequencies of future tokens.
    """

    @staticmethod
    def fft(t, x, wmin=0, wmax=None, c=10):
        dt = np.mean(t[1:]-t[:-1])
        w = np.fft.fftfreq(c * len(t), dt)
        y = np.fft.fft(x, n=c * len(t))
        if wmax == None:
            wmax = w.max()
        # y = np.abs(y)
        y = y[(w >= wmin) & (w <= wmax)]
        w = w[(w >= wmin) & (w <= wmax)]
        # not circle freqs
        return w, y

    @staticmethod
    def findmaxfreqs(w, spec):
        spec = np.abs(spec)
        kspec = []
        kw = []
        for idx in range(1, len(w) - 1):
            if spec[idx] > spec[idx - 1] and spec[idx] > spec[idx + 1]:
                kw.append(w[idx])
                kspec.append(spec[idx])
        return kw, kspec
    
    @staticmethod
    def sort_by_specter(w, spec, max_len=None):
        if max_len == None:
            max_len = len(spec)
        idxs = np.argsort(spec)[::-1][:max_len]
        sortspec = np.array([spec[i] for i in idxs])
        sortw = np.array([w[i] for i in idxs])
        return sortw, sortspec

    @staticmethod
    def choice_freqs(w, spec, pow=1, size=1):
        spec_sum = (spec**pow).sum()
        probabilities = list(map(lambda x: x**pow/spec_sum, spec))
        choice = np.random.choice(w, size=size, replace=False, p=probabilities)
        return choice      

    @staticmethod
    def find_dif_in_freqs(w, w0):
        for i in range(len(w)):
            for j in range(i + 1, len(w)):
                for freq0 in w0:
                    if abs((0.5 * (w[i] + w[j]) - freq0) / (0.5 * (0.5 * (w[i] + w[j]) + freq0))) < 0.05:
                        return abs(w[i] - w[j]) / 2
        return None

    @staticmethod
    def find_freq_for_summand(t, x, wmin=0, wmax=None, c=10, max_len=1, choice_size=1):
        w, s = FrequencyProcessor.fft(t, x, wmin, wmax, c)
        kw, ks = FrequencyProcessor.findmaxfreqs(w, s)
        kw, ks = FrequencyProcessor.sort_by_specter(kw, ks, max_len)
        out_freqs = FrequencyProcessor.choice_freqs(kw, ks, pow=max_len**0.5, size=choice_size)
        return out_freqs

    @staticmethod
    def find_freq_for_multiplier(t, x, w0, wmin=0, wmax=None, c=10, crit=0.9, crit_count=4):
        freqs, y = FrequencyProcessor.fft(t, x, wmin, wmax, c)
        y = np.abs(y)
        freqs, y = FrequencyProcessor.findmaxfreqs(freqs, y)
        freqs, y = FrequencyProcessor.keywsort(freqs, y, crit, crit_count)
        return FrequencyProcessor.find_dif_in_freqs(freqs, w0)

    # visualize
    @staticmethod
    def show_specter(freqs, spec, all=False):
        mx = np.abs(spec).max()
        plt.figure('specter')
        if all:
            plt.plot(freqs, spec.real, label='real')
            plt.plot(freqs, spec.imag, label='imag')
        plt.plot(freqs, np.abs(spec), label='abs')
        # plt.plot(freqs, np.angle(spec)/(2*np.pi), label='angle')
        plt.grid(True)
        plt.legend()
        plt.xlabel('w')
        plt.ylabel('S')
        plt.title('specter')




from scipy.optimize import differential_evolution
from FrequencyProcessor import *
from copy import deepcopy


class PeriodicTokenParamsOptimizer:
	"""
	Works with periodic simple token objects of the 'Function'class in Individ chromosome.
	Optimizes the parameters of the token for better approximation of input data.
	"""
	def __init__(self, individ, chromo_idx, optimizer='differential_evolution'):
		self.individ = individ
		self.chromo_idx = chromo_idx
		self.optimizer = optimizer
		self.token = self.individ.chromo[chromo_idx]

	@staticmethod
	def fitness_wrapper(params, *args):
		self, t, target_TS = args
		self.token.put_params(params)
		return self.individ.fitness(t, target_TS)	

	def optimize_params(self, t, target_TS, fix_each_function=True):
		if not self.token.fix:
			x = target_TS# - self.individ.value(t) + self.token.value(t)
			freq = self.token.params[1]
			eps = 0.005
			bounds = deepcopy(self.token.bounds)
			bounds[1] = (freq*(1-eps), freq*(1+eps))
			popsize = 7
			res = differential_evolution(self.fitness_wrapper, bounds, args=(self, t, x), popsize=popsize)
			self.token.put_params(res.x)
			self.token.fix = fix_each_function


		

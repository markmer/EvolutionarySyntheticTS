from FrequencyProcessor import FrequencyProcessor
import numpy as np

class TokenChoiser:
    """
    Works with the "Frequency Processor" class.
    After finding the frequency of the token, it selects the token depending on the situation.
    """

    @staticmethod
    def choice_token(t, target_TS, tokens, wmin=0, wmax=None, len_top_freqs=2, choice_size=1):
        Wmax = np.fft.fftfreq(len(t), np.mean(t[1:]-t[:-1])).max()
        if wmax == None:
            wmax = Wmax
        choice_freqs = FrequencyProcessor.find_freq_for_summand(t, target_TS, wmin, wmax, c=10, max_len=len_top_freqs, choice_size=choice_size)
        out_tokens = []
        for choice_freq in choice_freqs:
            if choice_freq < 0.001*Wmax:
                trend_tokens = list(filter(lambda x: x.name(False) == 'Power', tokens))
                if len(trend_tokens) > 0:
                    token = trend_tokens[0].copy()
                    out_tokens.append(token)
                    continue
            periodic_tokens = list(filter(lambda x: x.type == 'Periodic', tokens))
            token = np.random.choice(periodic_tokens).copy()
            token.set_param(1, choice_freq)
            out_tokens.append(token)
        return out_tokens


from PeriodicTokenParamsOptimizer import *
from NonPeriodicAllTokensParamsOptimizer import *
from ComplexImpTokenParamsOptimizer import *
from Individ import *


class AllTokensParamsOptimizer:

	def __init__(self, individ, full_imp_opt):
		self.individ = individ
		self.full_imp_opt = full_imp_opt

	def optimize_params(self, t, target_TS):
		for chromo_idx in range(len(self.individ.chromo)):
			gen = self.individ.chromo[chromo_idx]
			if not gen.fix:
				if gen.type == 'Periodic':
					opt = PeriodicTokenParamsOptimizer(self.individ, chromo_idx)
					opt.optimize_params(t, target_TS)
					if self.full_imp_opt:
						self.individ.imp_to_complex_imp(t, target_TS, stop_idx=chromo_idx)
						gen = self.individ.chromo[chromo_idx]
				# elif gen.type == 'Product':
				# 	opt = ProductTokenParamsOptimizer(self.individ, chromo_idx)
				# 	opt.optimize_params(t, target_TS)
				if gen.type == 'ImpComplex':
					opt = ComplexImpTokenParamsOptimizer(self.individ, chromo_idx)
					opt.optimize_params(t, target_TS)
				if gen.type == 'NonPeriodic':
					opt = NonPeriodicAllTokensParamsOptimizer(self.individ)
					opt.optimize_params(t, target_TS)
				
				# opt.optimize_params(t, target_TS)
				gen.fix = True

		# self.individ.del_low_functions(t)
		# self.individ.del_correlate_tokens(t)
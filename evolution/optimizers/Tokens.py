import random
import numpy as np
from copy import deepcopy
from scipy import signal
from functools import reduce
import matplotlib.pyplot as plt
from abc import ABC, abstractmethod, abstractproperty



class Token(ABC):
	"""
	Abstract class for tokens.
	Forms the main methods that must be implemented in the token for its operability.
	"""
	@abstractmethod
	def value(self, t):
		pass

	@abstractmethod
	def get_params(self):
		pass

	@abstractmethod
	def put_params(self, params):
		pass

	def name(self, with_params=False):
		try:
			if with_params:
				return type(self).__name__ + '(params=' + str(list(self.params)) + ')'
			return type(self).__name__
		except:
			return type(self).__name__

	def copy(self):
		return deepcopy(self)



class Function(Token):
	"""
	Inheritor of the 'Token'class.
	It is the parent class for tokens/functions of a simple type
	(for which it is enough to set parameters and a method for converting parameters to function values in the time axis, for example, Sin()).
	"""
	number_params = 0

	def __init__(self, params=None, bounds=None, fix=False):
		if bounds == None:
			self.bounds = [(0, 1) for i in range(self.number_params)]
		else:
			self.bounds = bounds
		if params == None:
			self.params = np.zeros(self.number_params)
			self.init_params()
		else:
			self.params = np.array(params, dtype=float)
		self.fix = fix
		self.fix_val = False

	def get_params(self):
		return self.params.copy()

	def put_params(self, params):
		self.params = np.array(params, dtype=float)
		self.fix_val = False
	
	def set_param(self, idx, param):
		self.params[idx] = param
		self.fix_val = False

	def init_params(self):
		for i in range(len(self.params)):
			self.params[i] = np.random.uniform(self.bounds[i][0], self.bounds[i][1])

	def show(self, t, with_params=False):
		plt.figure(type(self).__name__)
		plt.plot(t, self.value(t))
		plt.title(self.name(with_params))
		plt.legend()
		plt.grid(True)
		plt.xlabel('t, s')
		plt.ylabel('Time Series, o.e.')
		plt.show()
	
	def check_params(self):
		for idx, param in enumerate(self.params):
			if  (param > self.bounds[idx][1]):
				self.params[idx] = self.bounds[idx][1]
			if (param < self.bounds[idx][0]):
				self.params[idx] = self.bounds[idx][1]
	
	def value(self, t):
		if not self.fix_val:
			# self.check_params()
			self.fix_val = True
			self.val = self._val(self.params, t)
			# centralization
			self.val -= np.mean(self.val)
		return self.val
	
	@staticmethod
	def _val(params, t):
		pass





# Trend FunctionS
class Power(Function):
	number_params = 2

	def __init__(self, params=None, bounds=[(-1, 1), (0.1, 3)], fix=False):
		super().__init__(params=params, bounds=bounds, fix=fix)
		self.type = "NonPeriodic"

	@staticmethod
	def _val(params, t):
		return params[0]*t**params[1]



# Smooth Periodic Functions

class Sin(Function):
	number_params = 3

	def __init__(self, params=None, bounds=[(0, 1), (0, 0.5), (0, 1)], fix=False):
		super().__init__(params=params, bounds=bounds, fix=fix)
		self.type = "Periodic"
	
	@staticmethod
	def _val(params, t):
		return params[0]*np.sin(2*np.pi*(params[1]*t + params[2]))



# Pulses


class ImpSingle(Function):

	number_params = 6

	def __init__(self, params=None, bounds=[(0, 1), (0, 1000), (0, 100), (0, 100), (0, 3), (0, 3)], fix=False):
		super().__init__(params=params, bounds=bounds, fix=fix)
		self.type = "NonPeriodic"

	@staticmethod
	def _val(params, t):
		params[1:4] = np.abs(params[1:4])
		A, T1, T2, T3, p1, p2 = params

		cond1 = (t >= T1) & (t < T1 + T2)
		cond2 = (t >= T1 + T2) & (t <= (T1+T2+T3))
		m = np.zeros(len(t))
		if T2 != 0:
			m[cond1] = (np.abs(t[cond1] - T1)/T2)**np.abs(p1)
		if T3 != 0:
			m[cond2] = (np.abs(t[cond2] - (T1+T2+T3))/T3)**np.abs(p2)
		return 2*A*m


class Imp(Function):

	number_params = 7

	def __init__(self, params=None, bounds=[(-1, 1), (0, 0.5), (0.05, 0.98), (0.05, 0.98), (0, 3), (0, 3), (0, 1)], fix=False):
		super().__init__(params=params, bounds=bounds, fix=fix)
		self.type = "Periodic"

	@staticmethod
	def _val(params, t):
		A = params[0]
		fi = params[-1]

		T = 1/params[1]
		n1 = np.abs(params[2])
		T1 = n1*T
		n2 = np.abs(params[3])
		T2 = n2*(T - T1)
		T3 = T - T1 - T2

		p1 = params[4]
		p2 = params[5]

		t1 = (t + fi*T) % T

		cond1 = (t1 >= T1) & (t1 < T1 + T2)
		cond2 = (t1 >= T1 + T2) & (t1 <= T)
		try:
			m = np.zeros(len(t))
			if T2 != 0:
				m[cond1] = (np.abs(t1[cond1] - T1)/T2)**np.abs(p1)
			if T3 != 0:
				m[cond2] = (np.abs(t1[cond2] - T)/T3)**np.abs(p2)
		except:
			m = 0
			if cond1:
				m = np.abs((t1-T1)/T2)**np.abs(p1)
			elif cond2:
				m = np.abs((t1-T)/T3)**np.abs(p2)
		return 2*A*m


# тип соответствия и количество используемых временных параметров
imp_relations = {
	Imp: [ImpSingle, 3],
}


class ImpComplex(Token):
	"""
	This object has a complex structure and contains other tokens, so it is not inherited from 'Function'.
	Working with it is different from working with simple tokens.
	"""

	def __init__(self, pattern=Imp(), imps=None, fix=False):
		self.pattern = pattern.copy()
		self.number_params = self.pattern.number_params
		self.params = self.pattern.params
		self.fix = fix
		self.type = 'ImpComplex'
		if imps == None:
			imps = []
		self.imps = imps
		self.sample_imps = [self.pattern]
		self.value_type = 'norm'

	def name(self, with_params=False):
		if not with_params:
			return 'Complex' + self.pattern.name(with_params)
		s = type(self).__name__ + '(' + 'pattern=' + self.pattern.name(True) + ',imps=['
		for imp in self.imps:
			s += imp.name(True) + ','
		s = s[:-1]
		s += '])'
		return s

	def get_params(self):
		return self.pattern.get_params()

	def put_params(self, params):
		self.pattern.put_params(params)
	
	def get_imps_bounds(self, t):
		imps_bounds = deepcopy(self.pattern.bounds[:-1])
		imps_bounds[1] = (0, t.max())
		imps_bounds[2] = (0, imps_bounds[1][1]/4)
		imps_bounds[3] = (0, imps_bounds[1][1]/4)
		return imps_bounds

	def value(self, t):
		if self.value_type == 'norm':
			self.init_imps(t)
			return self.value_imps(t)
		else:
			return self.sample_value(t)

	def init_imps(self, t):
		if len(self.imps) != 0:
			return
		# определяем количество параметров отвечающих за время
		nb_of_time_params = imp_relations[type(self.pattern)][1]
		# выносим временные параметры
		T = list(map(lambda x: x, self.pattern.params[1: 1 + nb_of_time_params]))
		# преобразование периметра в частоту
		T[0] = 1/T[0]
		T1 = deepcopy(T)
		for idx in range(len(T)-1):
			T[idx] = T1[idx+1]*(T1[0] - np.sum(T[:idx]))
		T[-1] = T1[0] - np.sum(T[:-1])
		# выделяем полный период add (шаг), начальное значение(от фазы) и порог времени
		add = np.sum(T)
		sm = -self.pattern.params[-1]*add + T[0]
		mxt = t[-1]
		while sm < mxt:
			# расставляем в параметры амплитуду, точки старта sm, и длительности импульсов
			new_params = [self.pattern.params[0], sm]
			for i in range(nb_of_time_params - 1):
				new_params.append(T[1 + i])
			new_params.extend(self.pattern.params[nb_of_time_params+1: -1])
			# создаем импульс с нужными параметрами
			new_imp = imp_relations[type(self.pattern)][0](new_params)
			# new_imp.bounds = self.get_imps_bounds(t)
			self.imps.append(new_imp)
			sm += add

	def value_imps(self, t):
		return reduce(lambda val, x: val + x, list(map(lambda x: x.value(t), self.imps)))

	def init_params(self):
		self.pattern.init_params()

	def make_properties(self):
		mas = []
		for i in range(len(self.imps[0].params)):
			ps = np.array(list(map(lambda x: x.params[i], self.imps)))
			mas.append(ps)
		mas[1] = np.sort(mas[1])
		mas[1] = np.abs(mas[1][1:] - mas[1][:-1])
		return mas




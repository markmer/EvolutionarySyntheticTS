from sklearn.linear_model import Lasso, LinearRegression
from sklearn.preprocessing import normalize, scale
import numpy as np
# from Individ import *


class RegularizationRegressionOptimizer:
	"""
	Works with an object of the "Individual"class.
	Removes all insignificant tokens from an individual's chromosome.
	"""

	def __init__(self, individ):
		self.individ = individ
	
	def _preprocessing_data(self, t, _normalize=False):
		chromo = self.individ.get_chromo()
		features = np.transpose(np.array(list(map(lambda x: x.value(t), chromo))))
		if _normalize:
			# features, norms = normalize(features, norm='max', axis=0, return_norm=True)
			# return features, norms
			features = scale(features, axis=0)
		return features
	
	def _set_amplitudes_after_regression(self, coefs):#, norms):
		chromo = self.individ.get_chromo()
		for idx, gen in enumerate(chromo):
			if gen.type == 'Product':
				# gen.multipliers[0].params[0] *= coefs[idx]/norms[idx]
				gen.multipliers[0].set_param(0, gen.multipliers[0].params[0]*coefs[idx])#/norms[idx])
			elif gen.type == 'ImpComplex':
				for imp in gen.imps:
					# imp.params[0] *= coefs[idx]/norms[idx]
					imp.set_param(0, imp.params[0]*coefs[idx])#/norms[idx])
			else:
				gen.set_param(0, gen.params[0]*coefs[idx])#/norms[idx])
	
	def _del_tokens_with_zero_coef(self, coefs):
		new_chromo = []
		for idx, coef in enumerate(coefs):
			if coef != 0:
				new_chromo.append(self.individ.chromo[idx])
			else:
				# print(self.individ.chromo[idx], 'deleted')
				pass
		if len(new_chromo) == 0:
			max_idx = np.argmax(coefs)
			new_chromo.append(self.individ.chromo[max_idx])
		self.individ.chromo = new_chromo

	def linear_regression(self, t, target_TS):
		model = LinearRegression()
		features = self._preprocessing_data(t, False)
		model.fit(features, target_TS)
		self._set_amplitudes_after_regression(model.coef_)#, norms)

	def lasso(self, t, target_TS, alpha=0.001):
		if not self.individ.opt_by_lasso:
			self.individ.del_correlate_tokens(t)
			model = Lasso(alpha)
			target = scale(target_TS, axis=0)
			features = self._preprocessing_data(t, True)
			model.fit(features, target)
			# self._set_amplitudes_after_regression(model.coef_, norms)
			# self.individ.del_low_functions(t)
			self._del_tokens_with_zero_coef(model.coef_)
			self.linear_regression(t, target_TS)
			self.individ.opt_by_lasso = True


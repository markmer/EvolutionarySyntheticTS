import numpy as np
from Individ import *
from RegularizationRegressionOptimizer import *

class ComplexImpTokenParamsOptimizer:
	"""
	Works with an object of the 'ImpComplex'class.
	Optimizes the parameters of all objects of the 'ImpSingle' class for better approximation of input data.
	"""

	def __init__(self, individ, chromo_idx):
		self.individ = individ
		self.chromo_idx = chromo_idx
		self.token = self.individ.chromo[chromo_idx]

	@staticmethod
	def fitness_wrapper(params, *args):
		individ, t, target_TS, chromo_idx = args
		individ.chromo[chromo_idx].put_params(params)
		return individ.fitness(t, target_TS)

	def optimize_params_local(self, t, target_TS):
		for chromo_idx in range(len(self.token_individ.chromo)):
			gen = self.token_individ.chromo[chromo_idx]
			params0 = []
			if gen.type == 'NonPeriodic':
				params0.extend(gen.params)
				res = minimize(self.fitness_wrapper, params0, args=(self.token_individ, t, target_TS, chromo_idx), method='Nelder-Mead')
				gen.put_params(res.x)
	
	#--------------->exp
	@staticmethod
	def fitness_wrapper_all(params, *args):
		individ, t, target_TS= args
		individ.put_params(params)
		return individ.fitness(t, target_TS)

	def optimize_params_local_all(self, t, target_TS):
		params0 = self.token_individ.get_params()
		res = minimize(self.fitness_wrapper_all, params0, args=(self.token_individ, t, target_TS), method='Nelder-Mead')
		self.token_individ.put_params(res.x)
	#<------------------

	def get_pattern_value(self, t):
		model = self.token.pattern
		T = 1/model.params[1]
		T1 = model.params[2]*T
		T2 = T - T1
		fi = model.params[-1]
		pattern = model.value(t)[(t >= T - fi*T + T1) & (t <= T - fi*T + T1 + T2)]
		return pattern
	
	@staticmethod
	def find_pulse_starts(t, target_TS, pattern, T):
		cor = np.correlate(target_TS, pattern, mode='valid')#/np.var(pattern)
		cor /= np.max(np.abs(cor))
		# cor = target_TS
		# find maximums
		idxs = []
		for i in range(1, len(cor)-1):
			if cor[i] > cor[i-1] and cor[i] > cor[i+1] and cor[i] > 0:
				idxs.append(i)

		i = 1
		delta = 0.2
		while i < len(idxs):
			if (t[idxs[i]] - t[idxs[i-1]]) < (1-delta)*T:
				a = idxs[i] if cor[idxs[i]] < cor[idxs[i-1]] else idxs[i-1]
				idxs.remove(a)
			else:
				i += 1
		idxs = np.array(idxs)

		plt.figure('corr' + str(np.random.uniform()))
		plt.plot(target_TS)
		plt.plot(cor)
		tops = np.zeros(len(cor))
		tops[idxs] = cor[idxs]
		plt.plot(tops)
		plt.plot(pattern)

		return t[idxs]
	
	def form_init_imps_params(self, t, starts):
		self.token.init_imps(t)
		imp_pattern = self.token.imps[0]
		imps = []
		for start in starts:
			imps.append(imp_pattern.copy())
			imps[-1].params[1] = start
		self.token.imps = imps
	
	def del_imps_by_lasso(self, t, target_TS, alpha=0.1):
		ind = Individ(self.token.imps.copy())
		opt = RegularizationRegressionOptimizer(ind)
		opt.lasso(t, target_TS, alpha=alpha)
		self.token.imps = ind.chromo

	def optimize_params(self, t, target_TS):
		T = (1/self.token.pattern.params[1])
		pattern_val = self.get_pattern_value(t)
		starts = self.find_pulse_starts(t, target_TS, pattern_val, T)
		self.form_init_imps_params(t, starts)
		self.token_individ = Individ(chromo=self.token.imps, used_value='Plus', used_fitness=self.individ.fitness)
		self.optimize_params_local(t, target_TS)
		# self.optimize_params_local_all(t, target_TS)
		# self.del_imps_by_lasso(t, target_TS)
		self.token.fix = True


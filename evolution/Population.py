import sys, os
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from optimizers.Individ import *
from optimizers.AllTokensParamsOptimizer import *
from optimizers.RegularizationRegressionOptimizer import *
from optimizers.TokensBank import *


class Population:

	def __init__(self, population=None, tokens=None, population_size=5, alpha=0.001):
		if population == None:
			population = []
		self.population = population
		if tokens == None:
			raise "There is no tokens to build the expression"
		self.tokens = tokens
		self.population_size = population_size
		self.alpha = alpha

	def new_population(self, t, target_TS):
		for _ in range(self.population_size):
			new_ind = Individ(tokens=self.tokens)
			new_ind.init_formula(t, target_TS)
			self.population.append(new_ind)

	def crossover(self):
		parents = random.sample(self.population, len(self.population)//2)
		childs = parents[0].crossover(parents[1])
		self.population.extend(childs)

	def mutation(self, t, target_TS):
		mutants = []
		parents = random.sample(self.population, len(self.population)//2)
		for i in parents:
			mutants.extend(i.mutation(t, target_TS))
		self.population.extend(mutants)

	def optimize_params(self, t, target_TS, full_imp_opt=False):
		for ind in self.population:
			opt = AllTokensParamsOptimizer(ind, full_imp_opt)
			opt.optimize_params(t, target_TS)

			opt = RegularizationRegressionOptimizer(ind)
			opt.lasso(t, target_TS, self.alpha)

	def fitsort(self, t, target_TS):
		for i in self.population:
			i.fitness(t, target_TS)
		idx = np.argsort(list(map(lambda x: x.fit, self.population)))
		self.population = [self.population[i] for i in idx]

	def next_generation(self):
		fits = list(map(lambda x: x.fit, self.population))
		fits_sum = np.sum(fits)
		probabilities = list(map(lambda x: x/fits_sum, fits))
		for_remove = list(np.random.choice(self.population, size=len(self.population)-self.population_size, replace=False, p=probabilities))
		best = self.population[0]
		for ind in for_remove:
			self.population.remove(ind)
		if best not in self.population:
			self.population.append(best)

	def GA_step(self, t, target_TS):
		self.crossover()
		self.mutation(t, target_TS)
		self.optimize_params(t, target_TS)
		self.fitsort(t, target_TS)
		self.fits.append(self.population[0].fit)		

	def GA(self, t, target_TS, freqs_bounds, generations = 1, full_imp_opt=False):
		self.fits = []
		self.last_iteration = generations
		self.new_population(t, target_TS)
		self.optimize_params(t, target_TS, full_imp_opt)
		self.fitsort(t, target_TS)
		for i in range(1, generations):
			self.GA_step(t, target_TS)
			self.next_generation()


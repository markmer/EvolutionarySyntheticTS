import numpy as np
from optimizers.Individ import Individ
from scipy.stats import kde
from functools import reduce
import sys
from copy import deepcopy

# from accessify import protected, private



class Synthetic:
	"""
	Works with an object of the "Individual"class.
	Based on an expression from tokens, it generates synthetics using the "get_synthetic" method.
	"""

	def __init__(self, individ):
		self.individ = individ
		if type(individ).__name__ != 'Individ':
			raise 'Processing object must have class "Individ"'
		if len(individ.chromo) == 0:
			raise 'There is no expression here'
	
	def get_synthetic(self, t, residuals=None):
		"""
		Returns a synthetic time series of length t.

		Parameters

		t: 1D-array
			The timeline on which the synthetics will be generated.
		
		residuals: 1D-array
			Data for generating additive noise using the Monte Carlo method.
		"""
		val = self.get_synthetic_uncomplex_tokens(t, residuals)
		val += self.get_synthetic_complex_tokens(t)
		return val
	
	def get_synthetic_uncomplex_tokens(self, t, residuals=[], imp_noise=True):
		chromo = self.individ.get_chromo(('Periodic', 'NonPeriodic', 'Product'))
		if len(chromo) == 0:
			return np.zeros(len(t))
		if imp_noise:
			chromo = list(filter(lambda token: token.name(False) != 'Imp', chromo))
		ind = Individ(chromo)
		ind.unfix_all_tokens()
		if len(residuals) > 0:
			noise = Synthetic.montecarloRVS(residuals, len(t))
			val = ind.value(t) + noise
		else:
			val = ind.value(t)
		
		if imp_noise:
			imp_chromo = deepcopy(list(filter(lambda token: token.name(False) == 'Imp', self.individ.chromo)))
			if len(imp_chromo) != 0:
				ind = Individ(imp_chromo)
				ind.imp_to_complex_imp(t)
				synth = Synthetic(ind)
				val += synth.get_synthetic_complex_tokens(t, normal=True)
		return val
	
	def get_synthetic_complex_tokens(self, t, normal=False):
		# chromo = self.individ.get_chromo(('ImpComplex'))
		val = []
		clusters_cimps_with_close_freqs = self.clustering_imps()
		for tokens in clusters_cimps_with_close_freqs:
				val.append(Synthetic.get_synth_imps_in_complex(t, tokens, normal=normal))
		val.append(np.zeros(len(t)))
		return reduce(lambda x, y: x+y, val)
	
	@staticmethod
	def get_synth_imps_in_complex(t, tokens, normal=False):
		"""
		Generates sets of single pulses from clusters
		"""
		val = np.zeros(len(t))

		intervals_between_starts = []
		first_token = tokens[0]
		for token in tokens:
			intervals_between_starts.append(list(map(lambda x, y: x.params[1] - y.params[1], first_token.imps, token.imps)))

		start = np.abs(1/first_token.pattern.params[1])*first_token.pattern.params[-1]
		tmax = t[-1]
		dT0 = np.abs(1/first_token.pattern.params[1])
		while start < tmax:
			dT = np.random.normal(dT0, (0.005*dT0)**0.5)
			for idx, token in enumerate(tokens):
				token_start = start + np.random.choice(intervals_between_starts[idx])
				# we can also add a relationship between the impulses next to each other in the internal function
				val += Synthetic.get_synth_imp_in_complex(t, token, token_start, normal=normal)
			start += dT
		return val

	@staticmethod
	def get_synth_imp_in_complex(t, token, start, normal=False):
		"""
		Selects one of the single pulses to the starting point
		"""
		params = token.make_properties()
		param_sample = []
		quantiles = 0.7*np.ones(len(params))
		# quantiles[0] = 0.95
		# we choose the imp from imps of token
		imp = np.random.choice(token.imps).copy()
		# forming params for imp from distributions
		for idx, param_distr in enumerate(params):
			param_sample.append(Synthetic.montecarloRVS(param_distr-imp.params[idx], 1, quantiles[idx], normal=normal)[0])
		# change random (all) params in imp
		#idxs = np.arange(len(params))
		idxs = np.random.randint(0, 2, len(params))
		# narrowing the range for a smaller spread of params
		koef = 0.5
		imp.params[idxs] += koef*np.array(param_sample)[idxs]
		# narrow the time start of imp
		imp.set_param(1, start)
		return imp.value(t)

	@staticmethod
	def montecarloRVS(x, n=1, q=1, normal=False):
		x = x[(x <= np.quantile(x, q)) & (x >= np.quantile(x, 1-q))]
		if len(x) == 0:
			return np.zeros(n)
		if normal:
			var1 = 0.005*np.mean(x)
			var2 = np.var(x)
			sigm = max(var1, var2)**0.5
			return np.random.normal(np.mean(x), sigm, n)
		try:
			density = kde.gaussian_kde(x, bw_method='silverman')
		except:
			return np.random.choice(x, size=n, replace=True)
		xmin = np.min(x)
		xmax = np.max(x)
		xgrid = np.linspace(xmin, xmax, 1000)
		mx = np.max(density(xgrid))
		res = []
		count = 0
		while count < n:
			point = (np.random.uniform(xmin, xmax), np.random.uniform(0, mx))
			if density(point[0]) > point[1]:
				res.append(point[0])
				count += 1
		return np.array(res)

	def clustering_imps(self):
		"""
		Groups complex pulses according to the proximity of their frequencies
		"""
		complex_chromo = self.individ.get_chromo(['ImpComplex'])
		if len(complex_chromo) == 0:
			return []
		clusters = [[complex_chromo[0]]]
		for idx1 in range(1, len(complex_chromo)):
			flag = True
			for claster in clusters:
				w1 = complex_chromo[idx1].pattern.params[1]
				w2 = np.mean(list(map(lambda x: x.pattern.params[1], claster)))
				if 2*abs(w1 - w2)/(w1+w2) < 0.1:
					claster.append(complex_chromo[idx1])
					flag = False
			if flag:
				clusters.append([complex_chromo[idx1]])
		return clusters


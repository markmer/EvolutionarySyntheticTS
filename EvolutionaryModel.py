from evolution.Population import Population
from evolution.Synthetic import Synthetic
from evolution.optimizers.Tokens import *
from evolution.optimizers.Individ import *

class ExprModel:
    """
    This class is an interface for using the Population class (responsible for finding an approximating time series model using an evolutionary algorithm)
    and the Synthetics class, which is responsible for generating synthetics based on the resulting model.
    In the course of evolution, individuals-models-expressions develop. The resulting model is an object of the Individual class.
    The model is built from objects called tokens. Various functions (Sin, Pulse, Power) can be used as tokens.
    In order to avoid overfitting to all expressions in the course of evolution applies Lasso.
    For correct operation, it is recommended to set a set of tokens (it is better to leave the full set), as well as a set of hyperparameters.

    Example:
        t = np.arange(1, 1000, 1.)
        target = np.sin(t)
        target,_,_ = ExprModel.normalize(target)

        model = ExprModel()
        model.set_metaparams()
        expr, indo = model.fit(target)
        expr.show(t, target, with_params=True)

        s = expr.get_synthetic(len(t), residuals)

        model.write_expr('file.txt')
    """
    tokens = {
        'Power': Power(),
        'Sin': Sin(),
        'Imp': Imp()
    }

    def __init__(self):
        self.set_metaparams()
        self.expression = Individ()
        pass
        
    @abstractmethod
    def get_all_tokens():
        """
        Returns all available tokens.
        """
        return ExprModel.tokens.keys()
    
    def set_tokens(self, tokens=("Power", "Sin", "Imp")):
        """
        Set tokens.

        Parameters

        tokens: 1D-array, list, tuple
            Set of tokens.
        """
        self.tokens = {}
        for token in tokens:
            try:
                self.tokens[token] = ExprModel.tokens[token]
            except KeyError:
                raise KeyError('There are no such tokens') 
        
    def get_tokens(self):
        """
        Returns tokens.
        """
        return self.tokens
    
    def set_metaparams( self, Evolutionary_part = {'generations': 25, 'population_size': 6},
                        regularization_coef = 0.1,
                        full_imp_optimize = False):
        """
        Sets the metaparameters of the algorithm.

        Parameters

        Evolutionary_part: dict
                Set the "generation" and "population_size" fields to control the speed of search for solutions and their diversity.
        
        regularization_coef: float
                Sets the alpha coefficient for the Lasso. Controls the convergence rate and sensitivity to low-amplitude components.
        
        full_imp_optimize: bool
                This parameter defines an additional approximation of the pulses for each individual "pulse".
                Set True if you are sure that there are non-constant jumps in the series. It can SIGNIFICANTLY increase the execution time.
        
        return: None
        """
        self.Evolutionary_part = Evolutionary_part
        self.regularization_coef = regularization_coef
        self.full_imp_optimize = full_imp_optimize
    
    @abstractmethod
    def normalize(target):
        """
        Data normalization is a prerequisite for training. The average and norm can be added back after calculations.

        Parameters

        target: 1D-array
        
        return:
            norm_target: 1D-array
            mean: float
            norm: float
        """
        mean = np.mean(target)
        norm = np.max(np.abs(target-mean))
        return (target - mean)/norm, mean, norm
    
    def fit(self, target):
        """
        With the specified metaparameters, the model is trained on normalized data.

        Parameters

        target: 1D-array

        return: 
            expression:
                An expression is an Individual object that can be used to generate synthetics. It can be stored in text form as a formula.
            info: dict
                expression: string
                    Full formula of the object.
                fit: float
                    The quality of the approximation.
                residuals: 1D-array
                    It can be used to form the distribution of additive noise.
        """
        if len(self.tokens) == 0:
            raise 'There is no tokens here'
        pop = Population(   tokens=list(self.tokens.values()),
                            population_size=self.Evolutionary_part['population_size'],
                            alpha=self.regularization_coef)
        t = np.arange(1., len(target)+1, 1.)
        freqs = np.fft.fftfreq(len(t), 1.)
        freqs_bounds = [0, freqs.max()]
        pop.GA( t=t,
                target_TS=target,
                freqs_bounds=freqs_bounds,
                generations=self.Evolutionary_part['generations'],
                full_imp_opt=self.full_imp_optimize)
        self.expression = pop.population[0]
        best = self.expression
        info = {
            'expression': best.name(True),
            'fit': best.fit,
            'residuals': target - best.value(t)
        }
        return best, info
    
    def get_expr(self):
        """
        Get expression.
        """
        return self.expression

    def write_expr(self, file_name):
        """
        Write the expression in txt so that you can read and use it later.

        Parameters

        file_name: str
            Name of the file.
        """
        file = open('name', 'a')
        file.write(self.expression.name(True)+'\n')
        file.close
    
    def read_expr(self, file_name, idx=-1):
        """
        Read the expression from txt so that you can use it.

        Parameters

        file_name: str
            Name of the file.
        """
        file = open(file_name, 'r')
        res = file.read().splitlines()[idx]
        file.close()
        exec('self.expression=' + res)


    def get_synthetic(self, n, residuals=[]):
        """
        Generates synthetic data based on an expression.

        Parameters

        n: int
            Length of the synthetic.

        residuals: 1D-array
            Data for generating additive noise using the Monte Carlo method.

        return 1D-array
            Synthetic data. 
        """
        t = np.arange(1., n, 1.)
        synth = Synthetic(self.expression)
        val = synth.get_synthetic(t, residuals)
        return val





